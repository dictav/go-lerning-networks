/* TLSEchoClient
 */
package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"os"
	"time"
)

const entrustCert = `-----BEGIN CERTIFICATE-----
MIIBvzCCAWugAwIBAgIBATALBgkqhkiG9w0BAQUwMzEVMBMGA1UEChMMSmFuIE5l
d21hcmNoMRowGAYDVQQDExFqYW4ubmV3bWFyY2gubmFtZTAeFw0xNDAzMjYwMjE1
MjNaFw0xNTAzMjYwMjE1MjNaMDMxFTATBgNVBAoTDEphbiBOZXdtYXJjaDEaMBgG
A1UEAxMRamFuLm5ld21hcmNoLm5hbWUwXDANBgkqhkiG9w0BAQEFAANLADBIAkEA
+LWaSexpQtqnXq103d6JkwvYVcv4QRokSjgXQ6iBV+TQ0B5cjBFGIGtdXlrY9OdP
FvlaR2P0tXnM67SKeRNGbwIDAQABo2wwajAOBgNVHQ8BAf8EBAMCAKQwDwYDVR0T
AQH/BAUwAwEB/zANBgNVHQ4EBgQEAQIDBDAPBgNVHSMECDAGgAQBAgMEMCcGA1Ud
EQQgMB6CEWphbi5uZXdtYXJjaC5uYW1lgglsb2NhbGhvc3QwCwYJKoZIhvcNAQEF
A0EAlMZLY7C0HouJVPJfb2PEYyEiQuQUzrd9PZbfBBHQH3RKWbbAHuUIT08ntZoe
4VuVwbjj9u/JI3AcoJfK3Hq6KA==
-----END CERTIFICATE-----`

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: ", os.Args[0], "host:port")
		os.Exit(1)
	}
	service := os.Args[1]

	pool := x509.NewCertPool()
	ok := pool.AppendCertsFromPEM([]byte(entrustCert))
	if !ok {
		fmt.Println("append certs error")
	}

	conn, err := tls.Dial("tcp", service, &tls.Config{
		RootCAs: pool,
	})
	checkError(err)

	wEnd := make(chan bool)
	rEnd := make(chan bool)
	go func() {
		for n := 0; n < 10; n++ {
			time.Sleep(500 * time.Millisecond)
			fmt.Println("Writing...", n)
			conn.Write([]byte("Hello " + string(n+48)))
		}
		fmt.Println("Writing...end")
		wEnd <- true
	}()

	go func() {
		var buf [512]byte
		for {
			n, err := conn.Read(buf[0:])
			checkError(err)

			fmt.Println(string(buf[0:n]))

			select {
			case <-wEnd:
				rEnd <- true
			default:
			}
		}
	}()

	<-rEnd
}

func checkError(err error) {
	if err != nil {
		fmt.Println("client Fatal error ", err.Error())
		os.Exit(1)
	}
}
