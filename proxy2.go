/* SimpleEchoServer
 */
package main

import (
	"fmt"
	"net"
	"os"
)

func main() {

	//in
	service := ":1201"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	listenerIn, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	for {
		in, err := listenerIn.Accept()
		if err != nil {
			continue
		}
		// out
		//out, err := net.Dial("tcp", "192.168.56.101:6379")
		out, err := net.Dial("tcp", ":2000")
		checkError(err)

		go handleClient(in, out)
	}
}

func handleClient(in net.Conn, out net.Conn) {
	defer func() {
		in.Close()
		out.Close()
		fmt.Println("close")
	}()

	var inBuf [512]byte
	var outBuf [512]byte
	quit := make(chan bool)

	go func() {
		for {
			n1, err := in.Read(inBuf[0:])
			if err != nil {
				quit <- true
			}
			_, err2 := out.Write(inBuf[0:n1])
			if err2 != nil {
				quit <- true
			}
		}
	}()

	go func() {
		for {
			n2, err3 := out.Read(outBuf[0:])
			if err3 != nil {
				quit <- true
			}
			_, err4 := in.Write(outBuf[0:n2])
			if err4 != nil {
				quit <- true
			}
		}
	}()

	<-quit
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
