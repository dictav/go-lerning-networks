/* TLSEchoServer
 */
package main

import (
	"crypto/rand"
	"crypto/tls"
	"fmt"
	"net"
	"os"
	"runtime"
	"time"
)

var readCount int
var writeCount int

func main() {
	fmt.Printf("GOMAXPROCS is %d\n", runtime.GOMAXPROCS(0))

	listener, err := listener()
	checkError(err)
	fmt.Println("Listening")
	for {
		in, err := listener.Accept()
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		fmt.Println("Accepted")
		readCount = 0
		writeCount = 0

		out, err := net.Dial("tcp", ":6001")
		checkError(err)

		go handleClient(in, out)
	}
}

func listener() (net.Listener, error) {
	cert, err := tls.LoadX509KeyPair("jan.newmarch.name.pem", "private.pem")
	checkError(err)
	config := tls.Config{Certificates: []tls.Certificate{cert}}

	now := time.Now()
	config.Time = func() time.Time { return now }
	config.Rand = rand.Reader

	service := "0.0.0.0:1200"

	return tls.Listen("tcp", service, &config)
	//return net.Listen("tcp", service)
}

func handleClient(in net.Conn, out net.Conn) {
	defer func() {
		in.Close()
		out.Close()
		fmt.Println("close")
	}()

	var inBuf [512]byte
	var outBuf [512]byte
	quit := make(chan bool)

	go func() {
		for {
			n, err := in.Read(inBuf[0:])
			if err != nil {
				fmt.Println("Error read from client", err)
				quit <- true
			}

			readCount++
			fmt.Println("sent:", readCount)
			fmt.Println(string(inBuf[:n]))
			_, err = out.Write(inBuf[0:n])
			if err != nil {
				fmt.Println("Error write to redis", err)
				quit <- true
			}
		}
	}()

	go func() {
		for {
			n, err := out.Read(outBuf[0:])
			if err != nil {
				fmt.Println("Error read from redis", err)
				quit <- true
			}
			writeCount++
			fmt.Println("recieve:", writeCount)
			_, err = in.Write(outBuf[0:n])
			if err != nil {
				fmt.Println("Error write to client", err)
				quit <- true
			}
		}
	}()

	<-quit
}

func checkError(err error) {
	if err != nil {
		fmt.Println("server Fatal error ", err.Error())
		os.Exit(1)
	}
}
