/* SimpleEchoServer
 */
package main

import (
	"fmt"
	"net"
	"os"
)

func main() {

	//in
	service := ":1201"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	listenerIn, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// out
	service = ":6379"
	tcpAddr, err = net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	for {
		in, err := listenerIn.Accept()
		if err != nil {
			continue
		}
		out, err := net.Dial("tcp", "localhost:6379")
		checkError(err)

		go handleClient(in, out)
	}
}

func handleClient(in net.Conn, out net.Conn) {
	defer func(){
		in.Close()
		out.Close()
		fmt.Println("close")
	}

	var inBuf [512]byte
	var outBuf [512]byte

	for {
		n1, err := in.Read(inBuf[0:])
		if err != nil {
			return
		}
		fmt.Println(n1, string(inBuf[0:n1]))
		_, err2 := out.Write(inBuf[0:n1])
		if err2 != nil {
			return
		}
		if n1 == 512 {
			continue
		}

		for {
			n2, err3 := out.Read(outBuf[0:])
			if err3 != nil {
				return
			}
			fmt.Println("out", n2, string(outBuf[0:n2]))
			_, err4 := in.Write(outBuf[0:n2])
			if err4 != nil {
				return
			}
			if n2 != 512 {
				break
			}
		}
	}
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
