package main

import (
	"fmt"
	"math/rand"
)

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	quit := make(chan int)

	go func() {
		switch randInt
	}()

	for {
		select {
		case v := <- ch1:
			fmt.Println("ch1受信:", v)
		case v := <- ch2:
			fmt.Println("ch2受信:", v)
		case <- quit:
			fmt.Println("quit")
			return
		}
	}
}
